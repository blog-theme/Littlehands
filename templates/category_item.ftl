<!DOCTYPE html>
<html lang="zh-CN" data-default-color-scheme=auto>
<head>
    <meta charset="UTF-8">
    <#assign title="分类 - ${category.name} - ${config.name}" />
    <#include "layout/head.ftl" />
</head>
<body>
<#include "layout/header.ftl" />
<div id="body">
    <div class="app-container">
        <div class="row">
            <#include "layout/menu.ftl" />
            <div class="col-md-8 col-xs-12">
                <div class="card">
                    <h1 class="article-title">
                        <a href="${config.href}/category.html">分类</a>
                        <span> - </span>
                        <a href="${config.href}/category/${category.id}.html">${category.name}</a>
                    </h1>
                    <#list page.records as record>
                        <h3 class="item-h3">${record.year}</h3>
                        <#list record.items as item>
                            <div class="category-item">
                                <div class="category-item-title">
                                    <a href="${config.href}/article/${item.id}.html">${item.title}</a>
                                </div>
                                <div class="category-item-time">${item.year}-${item.month}-${item.day}</div>
                            </div>
                        </#list>
                    </#list>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <#include "layout/sidebar.ftl" />
            </div>
        </div>
    </div>
</div>
<#include "layout/footer.ftl" />
</body>
<#include "layout/script.ftl" />
</html>