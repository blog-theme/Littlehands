<!DOCTYPE html>
<html lang="zh-CN" data-default-color-scheme=auto>
<head>
    <meta charset="UTF-8">
    <#assign title="留言 - ${config.name}" />
    <#include "layout/head.ftl" />
</head>
<body>
<#include "layout/header.ftl" />
<div id="body">
    <div class="app-container">
        <div class="row">
            <#include "layout/menu.ftl" />
            <div class="col-md-8 col-xs-12">
                <div class="card">
                    <h1 class="article-title">
                        <a href="${config.href}/message.html">留言</a>
                    </h1>
                    <p>来说点什么吧~</p>
                </div>
                <div class="card">
                    <h2>共 ${message.total} 条留言</h2>
                    <#list message.records as record>
                        <div class="comment">
                            <div class="content">
                                <div class="pull-right text-muted">${record.createTime?string('yyyy-MM-dd')}</div>
                                <div><a href="${record.url}"><strong>${record.name}</strong></a></div>
                                <div class="text">${record.content}</div>
                            </div>
                        </div>
                    </#list>
                    <hr />
                    <#if message.total / message.size &gt; 0>
                        <div class="article-page">
                            <ul id="pager" class="pager" data-ride="pager"></ul>
                        </div>
                    </#if>
                </div>
                <div class="card">
                    <h2>新增留言</h2>
                    <form class="form-horizontal">
                        <div class="form-group row">
                            <div class="col-md-4 col-xs-12">
                                <input id="name" type="text" class="form-control" placeholder="您的昵称（必填）">
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <input id="email" type="email" class="form-control" placeholder="您的邮箱（选填，保密）">
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <input id="url" type="url" class="form-control" placeholder="网站链接（必填）">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <textarea id="name" class="form-control" rows="5" placeholder="描述，简介"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="text-align: right">
                                <button type="submit" class="btn btn-default" style="margin-right: 10px">提交</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <#include "layout/sidebar.ftl" />
            </div>
        </div>
    </div>
</div>
<#include "layout/footer.ftl" />
</body>
<#include "layout/script.ftl" />
<script type="application/javascript">
    // 手动进行初始化
    $('#pager').pager({
        page: ${message.current},
        recTotal: ${message.total},
        recPerPage: ${message.size},
        elements: ['prev', 'nav', 'next'],
        onPageChange: function (data) {
            let temps = window.location.href.split("?");
            let condition = {};
            if (temps.length > 1) {
                let params = temps[1].split('&');
                for (let param of params) {
                    let items = param.split("=");
                    condition[items[0]] = items[1];
                }
            }
            condition["pageNum"] = data.page;
            let cdt = [];
            for (let key in condition) {
                cdt.push(key + "=" + condition[key]);
            }
            window.location.href = temps[0] + '?' + cdt.join("&");
        }
    });
</script>
</html>