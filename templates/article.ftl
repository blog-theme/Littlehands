<!DOCTYPE html>
<html lang="zh-CN" data-default-color-scheme=auto>
<head>
    <meta charset="UTF-8">
    <#assign title="${article.title}" />
    <#include "layout/head.ftl" />
    <link rel="stylesheet" href="https://static.esion.xyz/Littlehands/css/comment.css">
</head>
<body>
<#include "layout/header.ftl" />
<div id="body">
    <div class="app-container">
        <div class="row">
            <#include "layout/menu.ftl" />
            <div class="col-md-12 col-xs-12">
                <article class="card article" itemprop="">
                    <header>
                        <h1 class="article-title"><a
                                    href="${config.href}/article/${article.id}.html">${article.title}</a></h1>
                        <ul class="article-meta">
                            <li itemprop="author">作者: <a itemprop="name" href="${config.href}/about.html"
                                                         rel="author">${author.name}</a></li>
                            <li>时间:
                                <time datetime="${article.createTime?string("yyyy-MM-dd HH:mm:ss")}"
                                      itemprop="datePublished">${article.createTime?string("yyyy-MM-dd")}</time>
                            </li>
                            <li>分类: ${article.categoryName}</li>
                        </ul>
                    </header>
                    <section class="content">
                        ${article.content}
                    </section>
                    <footer>
                        <#if article.tags?size &gt; 0>
                            <p class="pull-right text-muted">
                                <span>标签: </span>
                                <#list article.tags as tag>
                                    <a class="label label-primary" style="margin-right: 5px;color: #FFFFFF"
                                       href="${config.href}/search.html?title=${tag}">${tag}</a>
                                </#list>
                            </p>
                        </#if>
                        <p class="text-important">
                            <span>本博客所有文章除特别声明外，均采用 </span>
                            <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.zh" target="_blank">CC BY-SA
                                4.0</a>
                            <span> 协议 ，转载请注明出处！</span>
                        </p>
                        <#assign url="${config.href}/article/${article.id}.html">
                        <#assign pic="${article.image}">
                        <#if pic?length == 0>
                            <#assign pic="${config.background.article}">
                        </#if>
                        <p class="share">
                            <span>分享：</span>
                            <i class="icon icon-weibo" onclick="share_weibo()"></i>
                        </p>
                    </footer>
                </article>
                <div class="card">
                    <ul class="pager pager-justify">
                        <#if before??>
                            <li class="previous">
                                <a href="${config.href}/article/${before.id}.html">
                                    <i class="icon-arrow-left"></i>${before.title}</a>
                            </li>
                        <#else>
                            <li class="previous disabled">
                                <a>
                                    <i class="icon-arrow-left"></i>
                                    <span>没有了</span>
                                </a>
                            </li>
                        </#if>
                        <#if after??>
                            <li class="next">
                                <a href="${config.href}/article/${after.id}.html">
                                    <span>${after.title}</span>
                                    <i class="icon-arrow-right"></i>
                                </a>
                            </li>
                        <#else >
                            <li class="next disabled">
                                <a>
                                    <span>没有了</span>
                                    <i class="icon-arrow-right"></i>
                                </a>
                            </li>
                        </#if>
                    </ul>
                </div>
                <div class="card">
                    <#assign id="${article.id}" />
                    <#assign type=1>
                    <#include "layout/comment.ftl" />
                </div>
            </div>
        </div>
    </div>
</div>
<#include "layout/footer.ftl" />
</body>
<#include "layout/script.ftl" />
<script type="application/javascript">
</script>
</html>