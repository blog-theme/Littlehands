<!DOCTYPE html>
<html lang="zh-CN" data-default-color-scheme=auto>
<head>
    <meta charset="UTF-8">
    <#assign title="${page.title}" />
    <#include "layout/head.ftl" />
    <link rel="stylesheet" href="https://static.esion.xyz/Littlehands/css/comment.css">
</head>
<body>
<#include "layout/header.ftl" />
<div id="body">
    <div class="app-container">
        <div class="row">
            <#include "layout/menu.ftl" />
            <div class="col-md-12 col-xs-12">
                <article class="card article" itemprop="">
                    <header>
                        <h1 class="article-title"><a
                                    href="${config.href}/article/${page.id}.html">${page.title}</a></h1>
                        <ul class="article-meta">
                            <li itemprop="author">作者: <a itemprop="name" href="${config.href}/about.html"
                                                         rel="author">${author.name}</a></li>
                            <li>时间:
                                <time datetime="${page.createTime?string("yyyy-MM-dd HH:mm:ss")}"
                                      itemprop="datePublished">${page.createTime?string("yyyy-MM-dd")}</time>
                            </li>
                        </ul>
                    </header>
                    <section class="content">
                        ${page.content}
                    </section>
                    <footer>
                        <p class="text-important">
                            <span>本博客所有文章除特别声明外，均采用 </span>
                            <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.zh" target="_blank">CC BY-SA
                                4.0</a>
                            <span> 协议 ，转载请注明出处！</span>
                        </p>
                        <#assign url="${config.href}/article/${page.id}.html">
                        <#assign pic="${page.image}">
                        <#if pic?length == 0>
                            <#assign pic="${config.background.article}">
                        </#if>
                        <p class="share">
                            <span>分享：</span>
                            <i class="icon icon-weibo" onclick="share_weibo()"></i>
                        </p>
                    </footer>
                </article>
                <div class="card">
                    <#assign id="${page.id}" />
                    <#assign type=2>
                    <#include "layout/comment.ftl" />
                </div>
            </div>
        </div>
    </div>
</div>
<#include "layout/footer.ftl" />
</body>
<#include "layout/script.ftl" />
<script type="application/javascript">
</script>
</html>