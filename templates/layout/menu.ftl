<div class="col-md-12">
    <nav id="nav-menu">
        <#list menus as menu>
            <a <#if !menu.children?? && springMacroRequestContext.requestUri?ends_with(menu.data.href)>class="current" </#if>
                target="<#if menu.data.target == 0>_self<#else>_blank</#if>"<#if menu.children?? && menu.children?size &gt; 0> data-toggle="context-dropdown"<#else> href="${menu.data.href}"</#if>>
                <i class="${menu.data.icon}"></i>
                ${menu.data.name}
                <#if menu.children?? && menu.children?size &gt; 0> <span class="caret"></span></#if>
            </a>
            <#if menu.children?? && menu.children?size &gt; 0>
                <ul class="dropdown-menu">
                    <#list menu.children as child>
                        <li>
                            <a <#if springMacroRequestContext.requestUri?ends_with(child.data.href)>class="current"
                                </#if>href="${child.data.href}" 
                                <#if child.data.target == 0>target="_self"<#else>target="_blank" rel="noreferrer"</#if>>
                                <i class="${child.data.icon}"></i>
                                ${child.data.name}
                            </a>
                        </li>
                    </#list>
                </ul>
            </#if>
        </#list>
    </nav>
</div>