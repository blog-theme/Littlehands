<div id="bg"
     style="background: url('${config.background.index}') center center / cover no-repeat; transform: translate3d(0px, 0px, 0px);"></div>
<header id="top">
    <div class="app-container">
        <div id="top-title">
            <img src="${config.favicon}" alt="头像">
            <span>${config.index.title}</span>
        </div>
    </div>
</header>