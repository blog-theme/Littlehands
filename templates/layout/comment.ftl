<div class="comment-title" id="comments">已有 ${comment_total} 条评论</div>
<#list comments as comment>
    <div class="comment">
        <a href="${comment.website}" class="avatar">
            <img src="${comment.icon}">
        </a>
        <div class="content">
            <div class="pull-right text-muted">${comment.createTime?string("yyyy-MM-dd HH:mm")}</div>
            <div>
                <a href="${comment.website}"><strong>${comment.nickname}</strong></a>
                <#if comment.type == 1>
                    <span class="label label-primary">访客</span>
                <#elseif comment.type == 2>
                    <span class="label label-success">朋友</span>
                <#elseif comment.type == 3>
                    <span class="label label-warning">博主</span>
                </#if>
                <span class="label label-info">
                    <#if comment.browser?ends_with("Firfox")>
                        <i class="icon icon-firefox"></i>
                    <#elseif comment.browser?ends_with("Chrome")>
                        <i class="icon icon-chrome"></i>
                    <#elseif comment.browser?ends_with("Safari")>
                        <i class="icon icon-safari"></i>
                    </#if>
                    <#if comment.browser == "Unknown">未知<#else>${comment.browser}</#if>
                </span>
                <span class="label" style="margin-left: 3px;">
                    <#if comment.systemVersion?starts_with("Window")>
                        <i class="icon icon-windows"></i>
                    <#elseif comment.systemVersion?starts_with("Linux")>
                        <i class="icon icon-linux"></i>
                    </#if>
                    <#if comment.systemVersion != "Unknown">${comment.systemVersion}<#else>未知</#if>
                </span>
            </div>
            <div class="text">${comment.content}</div>
            <div class="actions">
                <a href="##"
                   class="reply-btn"
                   data-moveable="true"
                   data-toggle="modal"
                   data-target="#reply-dialog"
                   data-name="回复 ${comment.nickname}"
                   data-root-id="${comment.id}"
                   data-target-id="${comment.id}"
                >回复</a>
            </div>
        </div>
        <#if comment.children?size &gt; 0>
            <div class="comments-list">
                <#list comment.children as child>
                    <div class="comment">
                        <a href="${child.website}" class="avatar">
                            <img src="${child.icon}" alt="${child.nickname}">
                        </a>
                        <div class="content">
                            <div class="pull-right text-muted">${child.createTime?string("yyyy-MM-dd HH:mm")}</div>
                            <div>
                                <a href="${child.website}"><strong>${child.nickname}</strong></a>
                                <#if child.targetId &gt;0 >
                                    <span class="text-muted">回复</span>
                                    <a href="${child.targetWebsite}">${child.targetNickname}</a>
                                </#if><#if child.type == 1>
                                    <span class="label label-primary">访客</span>
                                <#elseif child.type == 2>
                                    <span class="label label-success">朋友</span>
                                <#elseif child.type == 3>
                                    <span class="label label-warning">博主</span>
                                </#if>
                                <span class="label label-info">
                                    <#if child.browser?ends_with("Firfox")>
                                        <i class="icon icon-firefox"></i>
                                    <#elseif child.browser?ends_with("Chrome")>
                                        <i class="icon icon-chrome"></i>
                                    <#elseif child.browser?ends_with("Safari")>
                                        <i class="icon icon-safari"></i>
                                    </#if>
                                    <#if child.browser == "Unknown">未知<#else>${child.browser}</#if>
                                </span>
                                <span class="label" style="margin-left: 3px;">
                                    <#if child.systemVersion?starts_with("Window")>
                                        <i class="icon icon-windows"></i>
                                    <#elseif child.systemVersion?starts_with("Linux")>
                                        <i class="icon icon-linux"></i>
                                    </#if>
                                    <#if child.systemVersion != "Unknown">${child.systemVersion}<#else>未知</#if>
                                </span>
                            </div>
                            <div class="text">${child.content}</div>
                            <div class="actions">
                                <a href="##"
                                   class="reply-btn"
                                   data-moveable="true"
                                   data-toggle="modal"
                                   data-target="#reply-dialog"
                                   data-name="回复 ${child.nickname}"
                                   data-root-id="${comment.id}"
                                   data-target-id="${child.id}"
                                >回复</a>
                            </div>
                        </div>
                    </div>
                </#list>
            </div>
        </#if>
    </div>
</#list>
<div class="comment-form">
    <div class="row">
        <div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
            <input id="comment-nickname" type="text" class="form-control" placeholder="称呼（必填）">
        </div>
        <div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
            <input id="comment-email" type="email" class="form-control" placeholder="电子邮件（必填，将保密）">
        </div>
        <div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
            <input id="comment-website" type="text" class="form-control" placeholder="网站（选填）">
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-md-12">
            <textarea id="comment-content" class="form-control new-comment-text" rows="5"
                      placeholder="撰写评论..."></textarea>
        </div>
    </div>
    <div class="row" style="text-align: right">
        <button class="btn btn-primary" style="margin-right: 10px" onclick="comment_submit()">评论</button>
    </div>
</div>
<div class="modal fade" id="reply-dialog">
    <div class="modal-dialog">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">×</span>
                <span class="sr-only">关闭</span>
            </button>
            <h4 class="modal-title" id="reply-title"></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
                    <input id="reply-nickname" type="text" class="form-control" placeholder="称呼（必填）">
                </div>
                <div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
                    <input id="reply-email" type="email" class="form-control" placeholder="电子邮件（必填，将保密）">
                </div>
                <div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
                    <input id="reply-website" type="text" class="form-control" placeholder="网站（选填）">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <textarea id="reply-content" class="form-control new-comment-text" rows="5"
                              placeholder="撰写评论..."></textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" style="margin-right: 10px" onclick="reply_submit()">回复</button>
        </div>
    </div>
</div>
<script type="application/javascript">
    let root_id = 0;
    let target_id = 0;
    window.onload = function () {
        $('.reply-btn').on('click', function (e) {
            let target = $(e.target);
            $("#reply-title").text(target.attr("data-name"));
            root_id = target.attr("data-root-id");
            target_id = target.attr("data-target-id");
        })
    }

    function reply_submit() {
        // 表单对象
        let replyNickname = $('#reply-nickname');
        let replyEmail = $('#reply-email');
        let replyWebsite = $('#reply-website');
        let replyContent = $('#reply-content');
        // 获取数据
        let nickname = replyNickname.val();
        if (nickname === '') {
            new $.zui.Messager('昵称不能为空', {
                type: 'danger',
                time: 3000
            }).show();
            return;
        }
        let email = replyEmail.val();
        if (email === '') {
            new $.zui.Messager('邮箱不能为空', {
                type: 'danger',
                time: 3000
            }).show();
            return;
        }
        let website = replyWebsite.val();
        let content = replyContent.val();
        if (content === '') {
            new $.zui.Messager('内容不能为空', {
                type: 'danger',
                time: 3000
            }).show();
            return;
        }
        // 发送请求
        send_comment({
            email: email,
            website: website,
            nickname: nickname,
            source_id: ${id},
            source_type: ${type},
            root_id: root_id,
            target_id: target_id,
            content: content
        })
        // 清空数据
        replyNickname.val('');
        replyEmail.val('');
        replyWebsite.val('');
        replyContent.val('');
        root_id = 0;
        target_id = 0;
        // 关闭对话框
        $("#reply-dialog").modal('hide');
    }

    function comment_submit() {
        // 表单对象
        let commentNickname = $('#comment-nickname');
        let commentEmail = $('#comment-email');
        let commentWebsite = $('#comment-website');
        let commentContent = $('#comment-content');
        // 获取数据
        let nickname = commentNickname.val();
        if (nickname === '') {
            new $.zui.Messager('昵称不能为空', {
                type: 'danger',
                time: 3000
            }).show();
            return;
        }
        let email = commentEmail.val();
        if (email === '') {
            new $.zui.Messager('邮箱不能为空', {
                type: 'danger',
                time: 3000
            }).show();
            return;
        }
        let website = commentWebsite.val();
        let content = commentContent.val();
        if (content === '') {
            new $.zui.Messager('内容不能为空', {
                type: 'danger',
                time: 3000
            }).show();
            return;
        }
        // 发送请求
        send_comment({
            email: email,
            website: website,
            nickname: nickname,
            source_id: ${id},
            source_type: ${type},
            content: content
        })
        // 清空数据
        commentNickname.val('');
        commentEmail.val('');
        commentWebsite.val('');
        commentContent.val('');
        root_id = 0;
        target_id = 0;
    }

    function send_comment(data) {
        $.ajax({
            url: '${config.href}/api/comment',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            dataType: "json",
            success: (res) => {
                if (res.code === 200) {
                    // 显示发送成功
                    new $.zui.Messager('评论成功，审核通过后显示', {
                        type: 'success',
                        time: 3000
                    }).show();
                }
            }
        })
    }
</script>
