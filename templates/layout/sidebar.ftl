<div class="card">
    <div class="input-control search-box has-icon-left has-icon-right"
         id="searchboxExample">
        <input id="search-input" type="search" class="form-control search-input"
               placeholder="搜索">
        <label for="search-input" class="input-control-icon-left search-icon"><i
                    class="icon icon-search"></i></label>
        <a href="#" class="input-control-icon-right search-clear-btn"><i
                    class="icon icon-remove"></i></a>
    </div>
</div>
<div class="card">
    <div class="author-avatar">
        <img src="${author.avatar}" alt="头像"/>
    </div>
    <div class="author-name"><a href="${config.href}/about.html">${author.name}</a></div>
    <div class="author-tag">
        <#list author.tags as tag>${tag}<#sep> / </#sep></#list>
    </div>
    <div class="author-account">
        <#list author.accounts as account>
            <a href="${account.href}" target="_blank" rel="noreferrer" class="btn btn-link" data-toggle="tooltip"
               data-placement="bottom" title="${account.name}">
                <i class="${account.icon}"></i>
            </a>
        </#list>
    </div>
</div>
<section class="card">
    <div class="widget-title">其他</div>
    <ul class="widget-content">
        <li><a href="${config.href}/rss.xml" target="_blank">文章RSS</a></li>
    </ul>
</section>