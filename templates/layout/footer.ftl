<footer id="bottom">
    <div>${config.index.description}</div>
    <div>
        <span>Powered by </span>
        <a href="https://gitee.com/qiaoshengda/blog" target="_blank" rel="noreferrer">blog</a>
        <span> , Theme by </span>
        <a href="https://moeshin.com/" target="_blank" rel="noreferrer">Littlehands</a>
        <span> ,</span>
    </div>
    <div>© 2021 <a href="${config.href}">${config.name}</a>. All right reserved.</div>
    <div>本站已接受${website.accessCount}人访问，已平稳运行${website.runTime}天</div>
    <#if config.keepOnRecord?length &gt; 0>
        <div><a href="https://beian.miit.gov.cn/" target="_blank" rel="noreferrer">${config.keepOnRecord}</a></div>
    </#if>
</footer>
<div id="to-top">
    <button class="btn btn-link" onclick="to_top()">
        <i class="icon icon-chevron-up"></i>
    </button>
</div>