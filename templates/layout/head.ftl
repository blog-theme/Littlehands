<meta charset="UTF-8">
<link rel="apple-touch-icon" sizes="76x76" href="${config.favicon}">
<link rel="icon" href="${config.favicon}">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
<meta name="theme-color" content="#2f4154">
<meta name="description" content="${author.description}">
<meta name="author" content="${author.name}">
<meta name="keywords" content="<#list config.keywords as keyword>${keyword}<#sep>,</#sep></#list>">
<meta property="og:type" content="website">
<meta property="og:title" content="${title}">
<meta property="og:url" content="${config.href}/page/link.html">
<meta property="og:site_name" content="${config.name}">
<meta property="og:description" content="${author.description}">
<meta property="og:locale" content="zh_CN">
<meta property="article:author" content="${author.name}">
<meta property="article:tag" content="<#list config.keywords as keyword>${keyword}<#sep>,</#sep></#list>">
<meta name="twitter:card" content="summary_large_image">
<meta name="generator" content="blog 1.0">
<title>${title} - ${config.name}</title>
<!-- 引入css -->
<!-- ZUI 标准版压缩后的 CSS 文件 -->
<link href="https://cdn.bootcdn.net/ajax/libs/zui/1.9.2/css/zui.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://static.esion.xyz/Littlehands/css/main.css">
<link rel="stylesheet" href="https://static.esion.xyz/Littlehands/css/QPlayer.min.css">
<!-- 主题依赖的图标库，不要自行修改 -->
<link rel="stylesheet" href="//at.alicdn.com/t/font_1749284_ba1fz6golrf.css">
<link rel="stylesheet" href="//at.alicdn.com/t/font_1736178_kmeydafke9r.css">