<!-- ZUI Javascript 依赖 jQuery -->
<script src="//cdn.bootcss.com/zui/1.10.0/lib/jquery/jquery.js"></script>
<#-- 在加载ZUI之前将图片属性替换 -->
<script type="application/javascript">
    // 为所有图片增加属性
    let img = $("article .content img");
    for (let index = 0; index < img.length; index++) {
        let item = $(img[index]);
        item.attr("data-toggle", "lightbox");
        item.attr("data-image", item.attr('src'));
        item.attr("data-group", "*");
        // 图片居中
        item.parent().css('text-align', 'center')
    }

    // 为所有的标题增加id
    function add_id(level) {
        let items = $("article .content h" + level);
        for (let i = 0; i < items.length; i++) {
            let item = $(items[i]);
            item.attr('id', item.text());
        }
    }

    add_id(1);
    add_id(2);
    add_id(3);
    add_id(4);
    add_id(5);
    add_id(6);
</script>
<!-- ZUI 标准版压缩后的 JavaScript 文件 -->
<script src="https://cdn.bootcdn.net/ajax/libs/zui/1.9.2/js/zui.min.js"></script>
<script src="https://static.esion.xyz/Littlehands/js/QPlayer.min.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/prettify/r298/prettify.min.js"></script>
<script type="application/javascript">
    // 作者信息提示
    $('[data-toggle="tooltip"]').tooltip();
    // 搜索栏
    $('#search-input').searchBox({
        escToClear: true, // 设置点击 ESC 键清空搜索框
        onPressEnter: function (event) {
            window.location.href = 'search.html?title=' + $(event.target).val();
        }
    });

    // 分享到微博
    function share_weibo() {
        let url = 'https://service.weibo.com/share/share.php';
        let params = {
            appkey: '',
            title: '分享《${title}》@${author.name} \n\n',
            url: '${url!""}',
            pic: '${pic!""}',
            searchPic: 'false',
            style: 'simple'
        };
        let param_list = [];
        for (let key in params) {
            param_list.push(key + '=' + encodeURIComponent(params[key]));
        }
        window.open(url + '?' + param_list.join('&'));
    }

    // QPlayer插件
    window.QPlayer.list = [
        <#list config.music as music>
        {
            "name": "${music.name}",
            "artist": "${music.artist}",
            "audio": "${music.url}",
            "cover": "${music.cover}",
            "lrc": "${music.lyric}"
        }<#sep>, </#sep>
        </#list>
    ];
    (function () {
        let q = window.QPlayer;
        q.isRotate = true;
        q.isShuffle = false;
        q.isAutoplay = false;
        q.isPauseOtherWhenPlay = true;
        q.isPauseWhenOtherPlay = true;
        q.$(function () {
            QPlayer.setColor("#66B3FF");
        });
    })();
    // 语法高亮
    (function () {
        $(".content pre").addClass("prettyprint linenums");
        prettyPrint();
    })();
    let is_show_top = false;
    $(window).on('load', function () {
        if (document.documentElement.scrollTop > 600) {
            show_to_top()
        }
        $(document).on('scroll', function (e) {
            if (document.documentElement.scrollTop > 600) {
                if (!is_show_top) {
                    show_to_top();
                }
            } else {
                if (is_show_top) {
                    close_to_top();
                }
            }
        })
    })

    function show_to_top() {
        is_show_top = true;
        $('#to-top').show();
    }

    function close_to_top() {
        is_show_top = false;
        $('#to-top').hide();
    }

    function to_top() {
        close_to_top();
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }

    let innerHeight = window.innerHeight;
    // 是否展示目录
    let is_show_catalog = false;

    function switch_category() {
        let $catalog_switch = $('#catalog-switch-i')[0];
        let $catalog = $('#catalog')[0];
        if (is_show_catalog) {
            // 隐藏
            $catalog_switch.classList.value = 'icon icon-list-ul';
            $catalog.style.right = '-200px';
        } else {
            $catalog_switch.classList.value = 'icon icon-times';
            $catalog.style.right = '0px';
        }
        is_show_catalog = !is_show_catalog;
    }

    // 文章的目录
    let catalog = $('<div id="catalog" style="top: ' +
        (innerHeight / 4).toFixed(0) +
        'px;height: ' + (innerHeight / 2).toFixed(0) +
        ';right: -200px"></div>')
    catalog.append($('<div class="catalog-switch" onclick="switch_category()"><i id="catalog-switch-i" class="icon icon-list-ul"></i></div>'));
    let catalog_content = $('<div class="catalog-content"></div>')
    let content = $('article .content');
    let count = 0;
    for (let item of content.children()) {
        if (item.tagName.startsWith('H') || item.tagName.startsWith('h')) {
            let $item = $(item);
            count++;
            catalog_content.append($('<div class="catalog-h' +
                item.tagName.charAt(1) +
                '"><a href="#' +
                $item.text() +
                '" onclick="switch_category()">' +
                $item.text() +
                '</a></div>'));
        }
    }
    if (count > 0) {
        catalog.append(catalog_content);
        $('body').append(catalog);
    }
</script>