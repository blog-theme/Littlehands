<!DOCTYPE html>
<html lang="zh-CN" data-default-color-scheme=auto>
<head>
    <meta charset="UTF-8">
    <#assign title="首页" />
    <#include "layout/head.ftl" />
</head>
<body>
<#include "layout/header.ftl" />
<div id="body">
    <div class="app-container">
        <div class="row">
            <#include "layout/menu.ftl" />
            <div class="col-md-8 col-xs-12">
                <#list page.records as  article>
                    <article class="card article" itemprop="">
                        <header>
                            <h1 class="article-title"><a
                                        href="${config.href}/article/${article.id}.html">${article.title}</a></h1>
                            <ul class="article-meta">
                                <li itemprop="author">作者: <a itemprop="name" href="${config.href}/about.html"
                                    rel="author">${author.name}</a></li>
                                <li>时间:
                                    <time datetime="${article.createTime?string("yyyy-MM-dd HH:mm:ss")}"
                                        itemprop="datePublished">${article.createTime?string("yyyy-MM-dd")}</time>
                                </li>
                                <li>分类: ${article.categoryName}</li>
                                <li itemprop="interactionCount">
                                    <a itemprop="discussionUrl" 
                                        href="${config.href}/article/${article.id}.html#comments">${article.commentCount}
                                        条评论</a>
                                </li>
                            </ul>
                        </header>
                        <section class="article-description">
                            ${article.description}
                        </section>
                    </article>
                </#list>
                <#if page.count &gt; 1>
                    <div class="article-page">
                        <ul id="pager" class="pager" data-ride="pager"></ul>
                    </div>
                </#if>
            </div>
            <div class="col-md-4 col-xs-12">
                <#include "layout/sidebar.ftl" />
            </div>
        </div>
    </div>
</div>
<#include "layout/footer.ftl" />
</body>
<#include "layout/script.ftl" />
<script type="application/javascript">
    // 手动进行初始化
    $('#pager').pager({
        page: ${page.pageNum},
        recTotal: ${page.total},
        recPerPage: ${page.pageSize},
        elements: ['prev', 'nav', 'next'],
        onPageChange: function (data) {
            let temps = window.location.href.split("?");
            let condition = {};
            if (temps.length > 1) {
                let params = temps[1].split('&');
                for (let param of params) {
                    let items = param.split("=");
                    condition[items[0]] = items[1];
                }
            }
            condition["pageNum"] = data.page;
            let cdt = [];
            for (let key in condition) {
                cdt.push(key + "=" + condition[key]);
            }
            window.location.href = temps[0] + '?' + cdt.join("&");
        }
    });
</script>
</html>