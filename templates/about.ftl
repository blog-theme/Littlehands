<!DOCTYPE html>
<html lang="zh-CN" data-default-color-scheme=auto>
<head>
    <meta charset="UTF-8">
    <#assign title="关于" />
    <#include "layout/head.ftl" />
</head>
<body>
<#include "layout/header.ftl" />
<div id="body">
    <div class="app-container">
        <div class="row">
            <#include "layout/menu.ftl" />
            <div class="col-md-8 col-xs-12">
                <div class="card">
                    ${author.content}
                </div>
            </div>
            <div class="col-md-4">
                <#include "layout/sidebar.ftl" />
            </div>
        </div>
    </div>
</div>
<#include "layout/footer.ftl" />
</body>
<#include "layout/script.ftl" />
<script type="application/javascript">
</script>
</html>