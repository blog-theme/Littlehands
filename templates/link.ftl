<!DOCTYPE html>
<html lang="zh-CN" data-default-color-scheme=auto>
<head>
    <meta charset="UTF-8">
    <#assign title="友链 - ${config.name}" />
    <#include "layout/head.ftl" />
</head>
<body>
<#include "layout/header.ftl" />
<div id="body">
    <div class="app-container">
        <div class="row">
            <#include "layout/menu.ftl" />
            <div class="col-md-8 col-xs-12">
                <div class="card">
                    <h1 class="article-title">
                        <a href="${config.href}/link.html">朋友</a>
                    </h1>
                    <h3 class="item-h3">友情，很重要</h3>
                    <div class="row" style="text-align: center">
                        <#list links as link>
                            <a target="_blank" href="${link.url}" title="${link.description}">
                                <div class="link-body">
                                    <img src="${link.icon}" alt="${link.name}">
                                    <p>${link.name}</p>
                                </div>
                            </a>
                        </#list>
                    </div>
                </div>
                <div class="card">
                    <h1 class="article-title">新增友链</h1>
                    <form class="form-horizontal">
                        <div class="form-group row">
                            <div class="col-md-4 col-xs-12">
                                <input id="name" type="text" class="form-control" placeholder="您的昵称（必填）">
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <input id="email" type="email" class="form-control" placeholder="您的邮箱（选填，保密）">
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <input id="url" type="url" class="form-control" placeholder="网站链接（必填）">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input id="icon" class="form-control" placeholder="网站图标，有QQ邮箱取QQ头像，否则取网站默认图标"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <textarea id="name" class="form-control" rows="5" placeholder="描述，简介"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="text-align: right">
                                <button type="submit" class="btn btn-default" style="margin-right: 10px">提交</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <#include "layout/sidebar.ftl" />
            </div>
        </div>
    </div>
</div>
<#include "layout/footer.ftl" />
</body>
<#include "layout/script.ftl" />
</html>