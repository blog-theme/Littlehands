<!DOCTYPE html>
<html lang="zh-CN" data-default-color-scheme=auto>
<head>
    <meta charset="UTF-8">
    <#assign title="分类 - ${config.name}" />
    <#include "layout/head.ftl" />
</head>
<body>
<#include "layout/header.ftl" />
<div id="body">
    <div class="app-container">
        <div class="row">
            <#include "layout/menu.ftl" />
            <div class="col-md-8 col-xs-12">
                <div class="card">
                    <h1 class="article-title">
                        <a href="${config.href}/category.html">分类</a>
                    </h1>
                    <#list categories as category>
                        <#if category.articles?size &gt; 0>
                            <h3 class="item-h3"><a href="${config.href}/category/${category.id}.html">${category.name}</a></h3>
                            <#list category.articles as article>
                                <div class="category-item">
                                    <div class="category-item-title">
                                        <a href="${config.href}/article/${article.id}.html">${article.title}</a>
                                    </div>
                                    <div class="category-item-time">${article.createTime?string("yyyy-MM-dd")}</div>
                                </div>
                            </#list>
                        </#if>
                        <#list category.children as child>
                            <#if child.articles?size &gt; 0>
                                <h3 class="item-h3"><a href="${config.href}/category/${child.id}.html">${child.name}</a></h3>
                                <#list child.articles as article>
                                    <div class="category-item">
                                        <div class="category-item-title">
                                            <a href="${config.href}/article/${article.id}.html">${article.title}</a>
                                        </div>
                                        <div class="category-item-time">${article.createTime?string("yyyy-MM-dd")}</div>
                                    </div>
                                </#list>
                            </#if>
                        </#list>
                    </#list>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <#include "layout/sidebar.ftl" />
            </div>
        </div>
    </div>
</div>
<#include "layout/footer.ftl" />
</body>
<#include "layout/script.ftl" />
</html>